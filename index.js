const electronPath = require('electron')
const spawn = require('child_process').spawn;
const path = require('path');
const fs = require('fs');

const rendererPath = path.resolve(__dirname, './renderer.js');

module.exports = ({url, options, postData}) => {
    return new Promise(resolve => {
        const electroArg = JSON.stringify({
            url,
            options,
            postData
        });
        const renderer = spawn(electronPath, [rendererPath, electroArg]);
        // let output;
        // renderer.stdout.on('data', data => {
            // output = output ? Buffer.concat([output, data]) : data;
			// console.log(output.length);
        // });
        renderer.stdout.on('end', () => {
			resolve(fs.readFileSync('./pdf-renderer-out.pdf'));
            //resolve(output);
        });
		//renderer.then(result => resolve(result.stdout));
    });
};
