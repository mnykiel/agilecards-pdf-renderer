const path = require('path');
const url = require('url');
const fs = require('fs');
const electron = require('electron').remote;

const BrowserWindow = electron.BrowserWindow;
// const app = electron.app;

const render = input => {
    return new Promise(resolve => {
        const window = new BrowserWindow({ width: 800, height: 600, show: false });

        const windowData = input.postData ? {
            extraHeaders: 'Content-Type: application/json',
            postData: [{
                type: 'rawData',
                bytes: Buffer.from(JSON.stringify(input.postData))
            }],
        } : undefined;

        window.loadURL(input.url, windowData);

        window.webContents.on('did-finish-load', () => {
            window.webContents.printToPDF(input.options, (error, data) => {
                if (error) {
                    throw error;
                }
                resolve(data);
                window.close();
                // process.stdout.write(data);
                // process.stdout.end();
                // fs.writeFileSync('./pdf-renderer-out.pdf', data);
                // setTimeout(() => app.quit(), 100);
            });
        });
    });
}
//
// app.on('ready', () => {
//     const input = JSON.parse(process.argv[2]);
//     render(input);
// });

module.exports = {
    render
}
