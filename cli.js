const pdfRenderer = require('./');

pdfRenderer({
    url: 'http://localhost:8080',
    options: {
        marginsType: 1,
        pageSize: {
            width: 62000,
            height: 100000
        }
    },
    postData: {
        issues: [
            {
                key: 'TEST'
            }
        ]
    }
}).then(result => process.stdout.write(result));
